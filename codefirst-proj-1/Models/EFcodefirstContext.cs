﻿using Microsoft.EntityFrameworkCore;

namespace codefirst_proj_1.Models
{
    public partial class EFcodefirstContext : DbContext
    {
        
        public EFcodefirstContext(DbContextOptions<EFcodefirstContext> options)
           : base(options)
        { }
        
        public virtual DbSet<User> Users { get; set; }
    }
}