﻿using System.ComponentModel.DataAnnotations;
namespace codefirst_proj_1.Models
{
    public class User 
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string Userame { get; set; }
        [StringLength(50)]
        public string Password { get; set; }
        [StringLength(50)]
        public string Phone { get; set; }
        [StringLength(50)]
        public string Email { get; set; }
        [StringLength(255)]
        public string Address { get; set; }
    }
}