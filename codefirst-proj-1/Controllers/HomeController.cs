﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using codefirst_proj_1.Models;
using Microsoft.EntityFrameworkCore;

namespace codefirst_proj_1.Controllers
{
    public class HomeController : Controller
    {
        private EFcodefirstContext _context;
        public IActionResult Index()
        {
            return View(_context.Users.ToList());
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}